/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasemagistral;

/**
 *
 * @author Martín Vargas
 */
public class metodo {
    
    public void ordenarInsercion(int[] array) {
        int aux;                 // variable que arrastra los valores      
        int cont1;               // variable contador 1, 
        int cont2;               // variable contador 2  

        /* Ciclo que se posiciona sobre el primer dato y se va moviendo a lo largo del array
        conforme acomoda las posiciones correctamente según el valor de los datos 
        por los cuales transcita
        */
        
        for (cont1 = 1; cont1 < array.length; cont1++) 
        {
            aux = array[cont1];
            for (cont2 = cont1 - 1; cont2 >= 0 && array[cont2] > aux; cont2--) {
                array[cont2 + 1] = array[cont2];
                array[cont2] = aux;
            }

        }

    }
    
}
